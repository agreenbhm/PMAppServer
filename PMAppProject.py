from flask import Flask, jsonify, abort, make_response, request
from flask.ext.restful import Api, Resource, reqparse, fields, marshal
from flask.ext.httpauth import HTTPBasicAuth
import os, sys, hashlib, pymssql, json, ast, cStringIO, ConfigParser
import time

app = Flask(__name__, static_url_path="")
api = Api(app)
auth = HTTPBasicAuth()
global serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal, webserverportGlobal, sslGlobal, sslcertGlobal, sslkeyGlobal

def con():
    server = serverGlobal
    port = dbportGlobal
    user = userGlobal
    password = passwordGlobal
    database = dbnameGlobal
    try:
        conn = pymssql.connect(
            server=server,
            port=port,
            user=user,
            password=password,
            database=database,
            as_dict=True)

        cur = conn.cursor()
        return { "Error": False, "Data": [conn, cur] }
    except Exception as e:
        return { "Error": True, "Data": e }

def dbEmployeeInOutStatusList(server, port, user, password, database):

    try:
        conn = pymssql.connect(
            server=server,
            port=port,
            user=user,
            password=password,
            database=database,
            as_dict=True)

        cursor = conn.cursor()

        statement = "SELECT EmpLName + ', ' + EmpFName AS FullName," \
                    "StatusLog.IOStatus,StatusLog.IOBackAt,StatusLog.IOComment,Empnum AS empnum," \
                    "CONVERT(VARCHAR(30), StatusLog.LastUpdated, 22) as LastUpdated" \
                    " FROM Employee INNER JOIN Office ON Employee.EmpOff=Office.OffId" \
                    " INNER JOIN (SELECT * FROM IOEmployeeLog) StatusLog ON Employee.Id=StatusLog.IOEmpID" \
                    " WHERE EmpStatus='A' AND OffName != ' No Selection'" \
                    " ORDER BY FullName"


        cursor.execute(statement)
        result = cursor.fetchall()

        for statusDict in result:
            statusDict['ValidStatuses'] = ''

        return result
    except:
        return {'error': True}


def dbEmployeeInOutStatus(server, port, user, password, database, empnum):
    try:
        conn = pymssql.connect(
            server=server,
            port=port,
            user=user,
            password=password,
            database=database,
            as_dict=True)

        cursor = conn.cursor()

        statement = "SELECT EmpLName + ', ' + EmpFName AS FullName," \
                    "IOEmployeeLog.IOStatus,IOEmployeeLog.IOBackAt," \
                    " IOEmployeeLog.IOComment, Empnum AS empnum," \
                    " CONVERT(VARCHAR(30), IOEmployeeLog.LastUpdated, 22) as LastUpdated," \
                    " Office.Offname as offname FROM Employee INNER JOIN" \
                    " IOEmployeeLog ON Employee.ID=IOEmployeeLog.IOEmpID" \
                    " INNER JOIN Office ON Employee.Empoff=Office.OffId" \
                    " WHERE Empnum='%s'" % empnum

        cursor.execute(statement)
        result = cursor.fetchone()

        statusesselect = "SELECT StatusDesc FROM IOLogStatus"

        cursor.execute(statusesselect)
        validstatuses = cursor.fetchall()
        statuslist = []
        for i in validstatuses:
            statuslist.append(i['StatusDesc'])
        result['ValidStatuses'] = statuslist

        return result

    except:
        result['error'] = True

def dbTimeGetClients():
    try:
        newCon = con()
        if not newCon["Error"]:
            conn, cur = newCon["Data"][0], newCon["Data"][1]
            statement = "SELECT ID, Cltnum, CltEng, Cltname, Cltsort, Cinvbill FROM dbo.Clients WHERE Deleted = 0"
            cur.execute(statement)
            result = cur.fetchall()
            #conn.commit()
            conn.close()
            return {"Error": False, "Data": result}

        else:
            return {"Error": True, "Data": newCon["Data"]}

    except Exception as e:
        return {'Error': True, "Data": e}

def dbTimeGetServices():
    try:
        newCon = con()
        if not newCon["Error"]:
            conn, cur = newCon["Data"][0], newCon["Data"][1]
            statement = "SELECT ID, SCCodeCat, SCCodeCode, SCDesc, SCBill, SCCodeDetail FROM dbo.ServiceCodes WHERE Deleted = 0"
            cur.execute(statement)
            result = cur.fetchall()
            #conn.commit()
            conn.close()
            return {"Error": False, "Data": result}
        else:
            return {"Error": True, "Data": newCon["Data"]}

    except Exception as e:
        return {'Error': True, "Data": e}


def dbWriteTEItem(TEEmpID, TEDate, TEExp, TEUnits, TEHours, TEDue,
                TECustomID, TECltID, TECltNum, TEEng, TECodeID, TEType, TESurchg, TEUserMod, TESource, TERef):
    try:
        newCon = con()
        if not newCon["Error"]:
            conn, cur = newCon["Data"][0], newCon["Data"][1]
            statement = """
            DECLARE	@return_value int
            DECLARE @EmpID int
            DECLARE @Emprate decimal(19,2)
            DECLARE @Fee decimal(19,2)
            DECLARE @SCRate int
            DECLARE @EmpRateCol varchar(20)
            DECLARE @EmprateText nvarchar(1000)
            DECLARE @CodeCat varchar(50)
            DECLARE @CodeSub varchar(50)
            DECLARE @CodeSer varchar(50)
            DECLARE @Billable int
            SELECT @EmpID= ID FROM [dbo].[Employee] WHERE Empnum = '%(TEEmpID)s'
            SELECT @Billable= SCBill FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s
            IF @Billable > 0
                BEGIN
                    SELECT @SCRate= SCRate FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s
                    SET @EmpRateCol='Emprate' + CAST(@SCRate as varchar)
                    SET @EmprateText= N'SELECT @retvalOUT = ' + @EmpRateCol + ' FROM [dbo].[Employee] WHERE ID = ' + CAST(@EmpID as varchar)
                    EXEC sp_executesql @EmprateText, N'@retvalOUT decimal(19,2) OUTPUT', @retvalOUT=@Emprate OUTPUT
                END
            ELSE
                BEGIN
                    SET @Emprate= 0.00
                END
            SELECT @Fee= %(TEHours)s * @Emprate
            SELECT @CodeCat= SCCodeCat FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s
            SELECT @CodeSub= SCCodeCode FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s
            SELECT @CodeSer= SCCodeDetail FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s


            EXEC	@return_value = [dbo].[vpm_WriteTEItem]
                    @TEEmpID = @EmpID,
                    @TEEmpIdOwn = @EmpID,
                    @TEIndicator = N'U',
                    @TEDate = '%(TEDate)s',
                    @TEFee = @Fee,
                    @TERate = @Emprate,
                    @TEExp = %(TEExp)s,
                    @TECost = @Fee,
                    @TEUnits = %(TEUnits)s,
                    @TEHours = %(TEHours)s,
                    @TEBillable = @Billable,
                    @TEDue = %(TEDue)s,
                    @TECustomID = %(TECustomID)s,
                    @TECltID = %(TECltID)s,
                    @TECltNum = '%(TECltNum)s',
                    @TEEng = %(TEEng)s,
                    @TECodeID = %(TECodeID)s,
                    @TECodeCat = @CodeCat,
                    @TECodeSub = @CodeSub,
                    @TECodeSer = @CodeSer,
                    @TEType = %(TEType)s,
                    @TESurchg = %(TESurchg)s,
                    @TEUserMod = %(TEUserMod)s,
                    @TESource = %(TESource)s,
                    @TERef = '%(TERef)s'

            SELECT	'Return Value' = @return_value
                    """ % {'TEDate': TEDate, 'TEExp': TEExp, 'TEUnits': TEUnits, 'TEHours': TEHours,
                           'TEDue': TEDue, 'TECustomID': TECustomID, 'TECltID': TECltID, 'TECltNum': TECltNum, 'TEEng': TEEng,
                           'TECodeID': TECodeID, 'TEType': TEType, 'TESurchg': TESurchg, 'TEUserMod': TEUserMod, 'TESource': TESource,
                           'TERef': TERef, 'TEEmpID': TEEmpID}


            cur.execute(statement)
            result = cur.fetchall()
            conn.commit()
            conn.close()
            return {"Error": False, "Data": result}

        else:
            return {"Error": True, "Data": newCon["Data"]}

    except Exception as e:
        print e
        return {'Error': True, "Data": e}

def dbUpdateTEItem(TEId, TEEmpID, TEDate, TEExp, TECost, TEUnits, TEHours, TEDue,
                  TECustomID, TECltID, TECltNum, TEEng, TECodeID, TEType, TESurchg, TEUserMod, TESource, TERef):


    try:
        newCon = con()
        if not newCon["Error"]:
            conn, cur = newCon["Data"][0], newCon["Data"][1]
            statement = """
            DECLARE	@return_value int
            DECLARE @EmpID int
            DECLARE @Emprate decimal(19,2)
            DECLARE @Fee decimal(19,2)
            DECLARE @SCRate int
            DECLARE @EmpRateCol varchar(20)
            DECLARE @EmprateText nvarchar(1000)
            DECLARE @CodeCat varchar(50)
            DECLARE @CodeSub varchar(50)
            DECLARE @CodeSer varchar(50)
            DECLARE @Billable int
            SELECT @EmpID= ID FROM [dbo].[Employee] WHERE Empnum = '%(TEEmpID)s'
            SELECT @Billable= SCBill FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s
            IF @Billable > 0
                BEGIN
                    SELECT @SCRate= SCRate FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s
                    SET @EmpRateCol='Emprate' + CAST(@SCRate as varchar)
                    SET @EmprateText= N'SELECT @retvalOUT = ' + @EmpRateCol + ' FROM [dbo].[Employee] WHERE ID = ' + CAST(@EmpID as varchar)
                    EXEC sp_executesql @EmprateText, N'@retvalOUT decimal(19,2) OUTPUT', @retvalOUT=@Emprate OUTPUT
                END
            ELSE
                BEGIN
                    SET @Emprate= 0.00
                END
            SELECT @Fee= %(TEHours)s * @Emprate
            SELECT @CodeCat= SCCodeCat FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s
            SELECT @CodeSub= SCCodeCode FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s
            SELECT @CodeSer= SCCodeDetail FROM [dbo].[ServiceCodes] WHERE ID = %(TECodeID)s


            UPDATE [dbo].[TimeEntry] SET
                    TEEmpID = @EmpID,
                    TEEmpIdOwn = @EmpID,
                    TEIndicator = N'U',
                    TEDate = '%(TEDate)s',
                    TEFee = @Fee,
                    TERate = @Emprate,
                    TEExp = %(TEExp)s,
                    TECost = %(TECost)s,
                    TEUnits = %(TEUnits)s,
                    TEHours = %(TEHours)s,
                    TEBillable = @Billable,
                    TEDue = %(TEDue)s,
                    TECustomID = %(TECustomID)s,
                    TECltID = %(TECltID)s,
                    TECltNum = '%(TECltNum)s',
                    TEEng = %(TEEng)s,
                    TECodeID = %(TECodeID)s,
                    TECodeCat = @CodeCat,
                    TECodeSub = @CodeSub,
                    TECodeSer = @CodeSer,
                    TEType = %(TEType)s,
                    TESurchg = %(TESurchg)s,
                    TEUserMod = %(TEUserMod)s,
                    TESource = %(TESource)s,
                    TERef = '%(TERef)s'
                    WHERE ID = %(TEId)s

            SELECT	'Return Value' = @return_value
                    """ % {'TEDate': TEDate, 'TEExp': TEExp, 'TECost': TECost, 'TEUnits': TEUnits, 'TEHours': TEHours,
                           'TEDue': TEDue, 'TECustomID': TECustomID, 'TECltID': TECltID, 'TECltNum': TECltNum, 'TEEng': TEEng,
                           'TECodeID': TECodeID, 'TEType': TEType, 'TESurchg': TESurchg, 'TEUserMod': TEUserMod, 'TESource': TESource,
                           'TERef': TERef, 'TEEmpID': TEEmpID, 'TEId': TEId}


            cur.execute(statement)
            conn.commit()
            conn.close()
            return {"Error": False, "Data": [{"TEId": TEId}]}

        else:
            return {"Error": True, "Data": newCon["Data"]}

    except Exception as e:
        print e
        return {'Error': True, "Data": e}

def dbMarkUpdateTable(TEId):
    try:
        newCon = con()
        if not newCon["Error"]:
            conn, cur = newCon["Data"][0], newCon["Data"][1]
            statement = """
                DECLARE	@return_value int

                EXEC	@return_value = [dbo].[vpm9_MarkUpdateTable]
                @TableName = N'TIMEENTRY',
                @Id = %s
                SELECT	'Return Value' = @return_value
                """ % (TEId)
            cur.execute(statement)
            result = cur.fetchall()
            conn.commit()
            conn.close()
            return {"Error": False, "Data": result}

        else:
            return {"Error": True, "Data": newCon["Data"]}

    except Exception as e:
        return {'Error': True, "Data": e}

def dbGetNextNumber(tableName):
    try:
        newCon = con()
        if not newCon["Error"]:
            conn, cur = newCon["Data"][0], newCon["Data"][1]
            statement = """
                DECLARE	@return_value int

                EXEC	@return_value = [dbo].[vpm8_GetNextNumber]
                @TableName = '%s'
                SELECT	'Return Value' = @return_value
                """ % (tableName)
            cur.execute(statement)
            result = cur.fetchall()
            conn.commit()
            conn.close()
            return {"Error": False, "Data": result}

        else:
            return {"Error": True, "Data": newCon["Data"]}

    except Exception as e:
        return {'Error': True, "Data": e}

def dbSetTEEntryNum(TEEntryNum, TEId):
    try:
        newCon = con()
        if not newCon["Error"]:
            conn, cur = newCon["Data"][0], newCon["Data"][1]
            statement = "UPDATE [dbo].[TimeEntry] SET TEEntryNum = %s WHERE ID = %s" % (TEEntryNum, TEId)
            cur.execute(statement)
            #result = cur.fetchall()
            conn.commit()
            conn.close()
            return {"Error": False, "Data": None}

        else:
            return {"Error": True, "Data": newCon["Data"]}

    except Exception as e:
        return {'Error': True, "Data": e}

def dbDeleteTimeEntry(TEId):
    try:
        newCon = con()
        if not newCon["Error"]:
            conn, cur = newCon["Data"][0], newCon["Data"][1]
            statement = "DELETE FROM TimeEntry WHERE ID = %s" % (TEId)
            cur.execute(statement)
            result = cur.fetchall()
            conn.commit()
            conn.close()
            return {"Error": False, "Data": result}

        else:
            return {"Error": True, "Data": newCon["Data"]}

    except Exception as e:
        return {'Error': True, "Data": e}

def dbReleaseTime(EmpId, IncludeAllTime, BegDate, EndDate):
        try:
            newCon = con()
            if not newCon["Error"]:
                conn, cur = newCon["Data"][0], newCon["Data"][1]
                statement = """
                    DECLARE	@return_value int
                    DECLARE @EmpID varchar(20)
                    SELECT @EmpID= ID FROM [dbo].[Employee] WHERE Empnum = '%s'

                    EXEC	@return_value = [dbo].[vpm_ReleaseTime]
                    @EmpId = @EmpID,
                    @OwnerId = @EmpID,
                    @IncludeAllTime = %s,
                    @BegDate = '%s',
                    @EndDate = '%s'

                    SELECT	'Return Value' = @return_value
                    """ % (EmpId, IncludeAllTime, BegDate, EndDate)
                cur.execute(statement)
                result = cur.fetchall()
                conn.commit()
                conn.close()
                return {"Error": False, "Data": result}

            else:
                return {"Error": True, "Data": newCon["Data"]}

        except Exception as e:
            return {'Error': True, "Data": e}

def dbEmployeeSetStatus(server, port, user, password, database, empstatus):
    try:
        conn = pymssql.connect(
            server=server,
            port=port,
            user=user,
            password=password,
            database=database,
            as_dict=True)

        cursor = conn.cursor()

        statusesselect = "SELECT StatusDesc FROM IOLogStatus"

        cursor.execute(statusesselect)
        statuses = cursor.fetchall()
        statuslist = []
        for i in statuses:
            statuslist.append(i['StatusDesc'])
        if empstatus['IOStatus'] in statuslist:
            empstatus['LastUpdated'] = time.strftime("%m/%d/%Y %H:%M:%S")
            updatestatement = "UPDATE IOEmployeeLogVar SET IOStatus='%s',IOBackAt='%s'," \
                        "IOComment='%s', LastUpdated='%s' FROM IOEmployeeLog IOEmployeeLogVar JOIN" \
                        " Employee EmployeeVar ON IOEmployeeLogVar.IOEmpID=EmployeeVar.ID" \
                        " WHERE EmployeeVar.Empnum='%s'" % (empstatus['IOStatus'], empstatus['IOBackAt'],
                            empstatus['IOComment'], empstatus['LastUpdated'], empstatus['empnum'])

            cursor.execute(updatestatement)
            rowcount = cursor.rowcount
            status = ""
            if rowcount > 1:
                conn.rollback()
                action = "rollback"
            elif rowcount == 1:
                conn.commit()
                action = "commit"
            else:
                conn.rollback
                action = "rollback-else"

            return action

        else:
            return "invalid-status"
    except:
        return "exception"


def dbAuthenticate(server, port, user, password, database, empnum, emppw):

    try:
        conn = pymssql.connect(
            server=server,
            port=port,
            user=user,
            password=password,
            database=database,
            as_dict=True)

        cursor = conn.cursor()

        statement = "SELECT RIGHT(Employee.Empssnum, 4) as ssn," \
                    "Employee.EmpPassword" \
                    " FROM Employee WHERE Empnum='%s'" % empnum

        cursor.execute(statement)
        result = cursor.fetchone()
        cipher = ''
        emptypw = False
        if result["EmpPassword"] == "########" or None:
            if len(result["ssn"] > 0):
                for i in range(0, len(result["ssn"])):
                    if i == 0 or i == 1 or i == 6 or i == 7:
                        cipher += (chr(ord(result["ssn"][i]) + 2))
                    elif i ==2 or i == 8:
                        cipher += (chr(ord(result["ssn"][i]) + 6))
                    elif i == 3 or i == 9:
                        cipher += (chr(ord(result["ssn"][i]) + 13))
                    elif i == 4:
                        cipher += (chr(ord(result["ssn"][i]) + 7))
                    else:
                        cipher += (chr(ord(result["ssn"][i])))
            else:
                emptypw = True
        else:
            cipher = result["EmpPassword"]
        
        correctpw = hashlib.sha1(empnum + cipher).hexdigest()
        authenticated = {"authenticated": False}
        if not emptypw:
            if correctpw == emppw:
                authenticated["authenticated"] = True

        return authenticated

    except Exception as e:
        print e
        authenticated = {"authenticated": False, "reason": "error"}
        return authenticated


inoutstatus_fields = {
    'FullName': fields.String,
    'IOComment': fields.String,
    'IOStatus': fields.String,
    'IOBackAt': fields.String,
    'empnum': fields.String,
    'ValidStatuses': fields.String,
    'LastUpdated': fields.String,
    'offname': fields.String,
    'uri': fields.Url('inoutstatus')
}

writeTE_fields = {
    'TEId': fields.Integer,
    'uri': fields.Url('writete')
}

releaseTE_fields = {
    'PostedCount': fields.Integer,
    'uri': fields.Url('releasete')
}
clients_fields = {
    'Cltnum': fields.String,
    'Cltname': fields.String,
    'CltEng': fields.String,
    'Cltsort': fields.String,
    'ID': fields.Integer,
    'Cinvbill': fields.Integer,
    'uri': fields.Url('clients')
}

services_fields = {
    'ID': fields.Integer,
    'SCCodeCat': fields.String,
    'SCCodeCode': fields.String,
    'SCDesc': fields.String,
    'SCBill': fields.String,
    'SCCodeDetail': fields.String,
    'uri': fields.Url('services')
}

authenticated_fields = {
    'authenticated': fields.String,
    'uri': fields.String
}

class ClientListAPI(Resource):

    def __init__(self):

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('empnum', type=str, location='json', required=True)
        self.reqparse.add_argument('emppw', type=str, location='json', required=True)
        super(ClientListAPI, self).__init__()

    def post(self):
        try:
            args = self.reqparse.parse_args()
            if "emppw" in args and "empnum" in args:
                empnum = args["empnum"]
                authenticated = dbAuthenticate(serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal,
                                               empnum, args["emppw"])
                if authenticated["authenticated"]:
                    clients = dbTimeGetClients()
                    if not clients["Error"]:
                        return {'Results': {'Clients': [marshal(client, clients_fields) for client in clients["Data"]], "Error": "False"}}
                    else:
                        abort(503)
                else:
                    abort(403)
            else:
                abort(403)
        except Exception as e:
            print e#str(e)
            abort(500)

class ServiceListAPI(Resource):

    def __init__(self):

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('empnum', type=str, location='json', required=True)
        self.reqparse.add_argument('emppw', type=str, location='json', required=True)
        super(ServiceListAPI, self).__init__()

    def post(self):
        try:
            args = self.reqparse.parse_args()
            if "emppw" in args and "empnum" in args:
                empnum = args["empnum"]
                authenticated = dbAuthenticate(serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal,
                                               empnum, args["emppw"])
                if authenticated["authenticated"]:
                    services = dbTimeGetServices()
                    if not services["Error"]:
                        return {'Results': {'Services': [marshal(service, services_fields) for service in services["Data"]]}}
                    else:
                        abort(503)
                else:
                    abort(403)
            else:
                abort(403)
        except Exception as e:
            print e#str(e)
            abort(500)

class WriteTimeAPI(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('empnum', type=str, location='json', required=True)
        self.reqparse.add_argument('emppw', type=str, location='json', required=True)
        self.reqparse.add_argument('TEId', type=int, location='json')
        self.reqparse.add_argument('TEDate', type=str, location='json', required=True)
        self.reqparse.add_argument('TEExp', type=float, location='json', required=True)
        #self.reqparse.add_argument('TECost', type=float, location='json', required=True)
        self.reqparse.add_argument('TEUnits', type=float, location='json', required=True)
        self.reqparse.add_argument('TEHours', type=float, location='json', required=True)
        self.reqparse.add_argument('TEDue', type=int, location='json', required=True)
        self.reqparse.add_argument('TECustomID', type=int, location='json', required=True)
        self.reqparse.add_argument('TECltID', type=int, location='json', required=True)
        self.reqparse.add_argument('TECltNum', type=str, location='json', required=True)
        self.reqparse.add_argument('TEEng', type=int, location='json', required=True)
        self.reqparse.add_argument('TECodeID', type=int, location='json', required=True)
        self.reqparse.add_argument('TEType', type=int, location='json', required=True)
        self.reqparse.add_argument('TESurchg', type=float, location='json', required=True)
        self.reqparse.add_argument('TEUserMod', type=int, location='json', required=True)
        self.reqparse.add_argument('TESource', type=int, location='json', required=True)
        self.reqparse.add_argument('TERef', type=str, location='json', required=True)
        super(WriteTimeAPI, self).__init__()

    def post(self):
        try:
            args = self.reqparse.parse_args()
        except Exception as e:
            print str(e) + " " + str(e.data)
            print request
        try:
            if "emppw" in args:
                authenticated = dbAuthenticate(serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal,
                                               args["empnum"], args["emppw"])
        except:
            abort(403)

        if authenticated["authenticated"]:

            try:
                error = False
                TEItem = dbWriteTEItem(TEEmpID=args["empnum"],
                                       TEDate=args["TEDate"],
                                       TEExp=args["TEExp"],
                                       TEUnits=args["TEUnits"],
                                       TEHours=args["TEHours"],
                                       TEDue=args["TEDue"],
                                       TECustomID=args["TECustomID"],
                                       TECltID=args["TECltID"],
                                       TECltNum=args["TECltNum"],
                                       TEEng=args["TEEng"],
                                       TECodeID=args["TECodeID"],
                                       TEType=args["TEType"],
                                       TESurchg=args["TESurchg"],
                                       TEUserMod=args["TEUserMod"],
                                       TESource=args["TESource"],
                                       TERef=args["TERef"])
                if not TEItem["Error"]:
                    TEId = TEItem["Data"][0]["TEId"]
                    UpdateTable = dbMarkUpdateTable(TEId)
                    if not UpdateTable["Error"]:
                        TEEntryNum = dbGetNextNumber("teentrynum")
                        if not TEEntryNum["Error"]:
                            SetTEEntryNumResult = dbSetTEEntryNum(TEEntryNum["Data"][0]["NextAvail"], TEId)
                            if SetTEEntryNumResult["Error"]:
                                error = True
                        else:
                            error = True
                    else:
                        error = True
                    if error:
                        DeleteResult = dbDeleteTimeEntry(TEId)
                        abort(500)
                    return {'status': marshal(TEItem["Data"][0], writeTE_fields)}
                else:
                    abort(500)
            except Exception as e:
                print e
                abort(500)
        else:
            abort(403)


    def put(self):
        args = self.reqparse.parse_args()
        try:
            if "emppw" in args:
                authenticated = dbAuthenticate(serverGlobal, dbportGlobal, userGlobal, passwordGlobal,
                                               dbnameGlobal,
                                               args["empnum"], args["emppw"])
        except:
            abort(403)

        if authenticated["authenticated"]:
            if "TEId" in args:
                try:
                    error = False
                    TEItem = dbUpdateTEItem(TEEmpID=args["empnum"],
                                           TEDate=args["TEDate"],
                                           TEExp=args["TEExp"],
                                           TEUnits=args["TEUnits"],
                                           TEHours=args["TEHours"],
                                           TEDue=args["TEDue"],
                                           TECustomID=args["TECustomID"],
                                           TECltID=args["TECltID"],
                                           TECltNum=args["TECltNum"],
                                           TEEng=args["TEEng"],
                                           TECodeID=args["TECodeID"],
                                           TEType=args["TEType"],
                                           TESurchg=args["TESurchg"],
                                           TEUserMod=args["TEUserMod"],
                                           TESource=args["TESource"],
                                           TERef=args["TERef"],
                                           TEId=args["TEId"])
                    if not TEItem["Error"]:
                        return {'status': marshal(TEItem["Data"][0], writeTE_fields)}
                    else:
                        abort(500)
                except Exception as e:
                    print e
                    abort(500)
            else:
                abort(500)
        else:
            abort(403)

class ReleaseTimeAPI(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('empnum', type=str, location='json', required=True)
        self.reqparse.add_argument('emppw', type=str, location='json', required=True)
        self.reqparse.add_argument('BegDate', type=str, location='json', required=True)
        self.reqparse.add_argument('EndDate', type=str, location='json', required=True)
        self.reqparse.add_argument('IncludeAllTime', type=int, location='json', required=True)
        super(ReleaseTimeAPI, self).__init__()

    def post(self):
        args = self.reqparse.parse_args()
        try:
            if "emppw" in args:
                authenticated = dbAuthenticate(serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal,
                                               args["empnum"], args["emppw"])
        except:
            abort(403)

        if authenticated["authenticated"]:

            try:
                TERelease = dbReleaseTime(EmpId=args["empnum"],
                                          IncludeAllTime=args["IncludeAllTime"],
                                          BegDate=args["BegDate"],
                                          EndDate=args["EndDate"])

                if not TERelease["Error"]:
                    return {'status': marshal(TERelease["Data"][0], releaseTE_fields)}
                else:
                    abort(500)
            except Exception as e:
                abort(500)
        else:
            abort(403)


class EmployeeInOutStatusListAPI(Resource):

    def __init__(self):

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('empnum', type=str, location='json', required=True)
        self.reqparse.add_argument('emppw', type=str, location='json', required=True)
        super(EmployeeInOutStatusListAPI, self).__init__()

    def post(self):
        try:
            args = self.reqparse.parse_args()
            if "emppw" in args and "empnum" in args:
                empnum = args["empnum"]
                authenticated = dbAuthenticate(serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal, empnum, args["emppw"])
                if authenticated["authenticated"]:
                    return {'statuses': [marshal(emp, inoutstatus_fields) for emp in dbEmployeeInOutStatusList(
                        serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal)]}
                else:
                    abort(403)
            else:
                abort(403)
        except:
            abort(500)

class Authenticate(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('empnum', type=str, location='json', required=True)
        self.reqparse.add_argument('emppw', type=str, location='json', required=True)
        super(Authenticate, self).__init__()

    def post(self):
        try:
            print "Pre-Arg Parse"
            args = self.reqparse.parse_args()
            authpost = {}
            #print args
            print "Pre-Arg For"
            for k, v in args.items():
                if v is not None:
                    authpost[k] = v
            #print authpost
            if "empnum" in authpost and "emppw" in authpost:
                authstatus = dbAuthenticate(serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal,
                                            authpost["empnum"], authpost["emppw"])
                if not authstatus["authenticated"]:
                    abort(403)

                elif authstatus["authenticated"]:
                    authstatus["uri"] = request.url
                    return {'auth': marshal(authstatus, authenticated_fields)}

            else:
                abort(403)
        except Exception as e:
            print "Auth Internal Error"
            print e
            abort(500)

class EmployeeInOutStatusAPI(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('empnum', type=str, location='json', required=True)
        self.reqparse.add_argument('emppw', type=str, location='json', required=True)
        self.reqparse.add_argument('IOBackAt', type=str, location='json')
        self.reqparse.add_argument('IOComment', type=str, location='json')
        self.reqparse.add_argument('IOStatus', type=str, location='json')
        self.reqparse.add_argument('LastUpdated', type=str, location='json')
        super(EmployeeInOutStatusAPI, self).__init__()


    def post(self, empnum):
        try:
            args = self.reqparse.parse_args()
            if "emppw" in args:
                authenticated = dbAuthenticate(serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal, empnum, args["emppw"])
                if authenticated["authenticated"]:
                    empstatus = dbEmployeeInOutStatus(
                        serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal, empnum)
                    if empstatus is None:
                        abort(500)
                    elif "error" in empstatus:
                        if empstatus['error']:
                            abort(500)
                    else:
                        return {'status': marshal(empstatus, inoutstatus_fields)}
                else:
                    abort(403)
            else:
                abort(403)
        except:
            abort(500)

    def put(self, empnum):

        try:
            args = self.reqparse.parse_args()
            if "emppw" in args:
                authenticated = dbAuthenticate(serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal, empnum, args["emppw"])
                if authenticated["authenticated"]:
                    empstatus = dbEmployeeInOutStatus(
                        serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal, empnum)

                    if empstatus is None:
                        abort(500)
                    elif "error" in empstatus:
                        if empstatus['error']:
                            abort(500)
                    else:
                        args = self.reqparse.parse_args()
                        for k, v in args.items():
                            if v is not None:
                                empstatus[k] = v

                        action = dbEmployeeSetStatus(
                            serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal, empstatus)

                        if action == "commit":
                            empstatus = dbEmployeeInOutStatus(
                                serverGlobal, dbportGlobal, userGlobal, passwordGlobal, dbnameGlobal, empnum)
                            return {'status': marshal(empstatus, inoutstatus_fields)}
                        else:
                            abort(500)
                else:
                    abort(403)
            else:
                abort(403)
        except:
            abort(500)

api.add_resource(EmployeeInOutStatusAPI, '/api/v1.0/inoutstatus/<string:empnum>', endpoint='inoutstatus')
api.add_resource(EmployeeInOutStatusListAPI, '/api/v1.0/inoutstatus', endpoint='inoutstatuslist')
api.add_resource(Authenticate, '/api/v1.0/auth', endpoint='auth')
api.add_resource(WriteTimeAPI, '/api/v1.0/writete', endpoint='writete')
api.add_resource(ReleaseTimeAPI, '/api/v1.0/releasete', endpoint='releasete')
api.add_resource(ClientListAPI, '/api/v1.0/clients', endpoint='clients')
api.add_resource(ServiceListAPI, '/api/v1.0/services', endpoint='services')

def invalidConfig(configFile, invalid=None):

        if invalid == 'missing':
            print 'No configuration file found at ' + configFile
        else:
            print 'Invalid configuration file found at ' + configFile
            print 'Invalid field: ' + invalid

        print ''
        print 'Please create file with the following parameters:'
        print ''
        print 'server=Practice Management server\SQL instance name'
        print 'port=SQL port (defaults to 1433 if not present)'
        print 'webserverport=port to run web server on (defaults to 80 if not present, 443 if SSL enabled)'
        print 'ssl=enable SSL (true or false)'
        print 'cert=path to SSL cert in PEM format (only required if SSL enabled)'
        print 'key=path to SSL key in PEM format (only required if SSL enabled)'
        print 'username=database user'
        print 'password=database password'
        print 'dbname=Database name'
        exit(1)


if __name__ == '__main__':
    #
    configFile = os.path.dirname(sys.argv[0]) + '/pmappserver.config'
    if os.name == 'posix':
        if os.path.exists('/etc/pmappserver.config'):
            configFile = '/etc/pmappserver.config'

    configFile = os.path.dirname(sys.argv[0]) + '/pmappserver.config'

    if os.path.exists(configFile):
        configData = '[root]\n' + open(configFile, 'r').read()
        configFP = cStringIO.StringIO(configData)
        cp = ConfigParser.RawConfigParser()
        cp.readfp(configFP)

        if cp.has_option('root','server'):
            serverGlobal = cp.get('root', 'server')
        else:
            invalidConfig(configFile, 'server')

        if cp.has_option('root','dbport'):
            dbportGlobal = int(cp.get('root', 'port'))
        else:
            dbportGlobal = 1433

        if cp.has_option('root', 'cert'):
            sslcertGlobal = cp.get('root', 'cert')
            if not os.path.exists(sslcertGlobal):
                invalidConfig(configFile, 'cert')
        else:
            invalidConfig(configFile, 'cert')

        if cp.has_option('root', 'key'):
            sslkeyGlobal = cp.get('root', 'key')
            if not os.path.exists(sslkeyGlobal):
                invalidConfig(configFile, 'key')
        else:
            invalidConfig(configFile, 'key')

        if cp.has_option('root','webserverport'):
            webserverportGlobal = int(cp.get('root', 'webserverport'))
        else:
             webserverportGlobal = 443

        if cp.has_option('root','username'):
            userGlobal = cp.get('root', 'username')
        else:
            invalidConfig(configFile, 'username')

        if cp.has_option('root','password'):
            passwordGlobal = cp.get('root', 'password')
        else:
            invalidConfig(configFile, 'password')

        if cp.has_option('root','dbname'):
            dbnameGlobal = cp.get('root', 'dbname')
        else:
            invalidConfig(configFile, 'dbname')

        if cp.has_option('root','debug'):
            debug = cp.get('root', 'debug')
            if debug == 'True':
                debug = True
                debugFile = os.path.dirname(os.path.abspath(sys.argv[0])) + '/pmappserver.log'
                os.environ['TDSDUMP'] = debugFile
        else:
            debug = False

        #if sslGlobal == 'True':
        print "Running SSL"
        app.run(debug=debug, host='0.0.0.0', port=webserverportGlobal, ssl_context=(sslcertGlobal, sslkeyGlobal))
        #else:
        #    app.run(debug=debug, host='0.0.0.0', port=webserverportGlobal)

    if not os.path.exists(configFile):
        invalidConfig(configFile, 'missing')

