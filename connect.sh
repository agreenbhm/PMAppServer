#!/bin/bash

cd /usr/src/env
kill $(ps aux | grep [p]ython | awk '{print $2}')
cp /usr/src/pmapp/PMAppProject.py /usr/src/env/PMAppProject.py
source /usr/src/env/bin/activate
/usr/src/env/startup.sh
